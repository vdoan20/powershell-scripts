﻿$date = Get-Date
$timeframe = $date.addhours(-6)

$username = Read-host "Enter user's name"

Get-EventLog -ComputerName wdc-dc-01 -LogName Security -EntryType FailureAudit -After $timeframe | where {$_.Message -like '*'+$($username)+'*'} | Select TimeGenerated,Message | FL

#Get-EventLog -ComputerName wdc-dc-01 -LogName Security -EntryType FailureAudit -After $timeframe | Select TimeGenerated