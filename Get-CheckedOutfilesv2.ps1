﻿Import-Module C:\SPOMod\SPOModCURRENT20170918.psm1

$User = "mbamba@earthnetworks.com"
$SiteURL = "https://earthnetworks1-admin.sharepoint.com/sites/home"

Add-Type -Path "C:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.dll"
Add-Type -Path "C:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.Runtime.dll"

Connect-SPOCSOM -Username $User -Url https://earthnetworks1.sharepoint.com/sites/home/WBHOps

$areas = Get-SPOList

$menu = @{}

for ($i=1;$i -le $areas.count; $i++)
{ Write-Host "$i. $($areas[$i-1].Title)"
$menu.Add($i,($areas[$i-1].Title)) }

[int]$ans = Read-Host 'Enter selection'
$selection = $menu.Item($ans) ; 

Get-SPOListItems $selection -IncludeAllProperties $true -Recursive | where {($_.FSObjType -eq 0) -and ($_.CheckoutUser -ne $null)} | select FileRef,CheckoutUser