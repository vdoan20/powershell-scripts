﻿Import-Module C:\SPOMod\SPOModCURRENT20170918.psm1

$User = "mbamba@earthnetworks.com"
$SiteURL = "https://earthnetworks1-admin.sharepoint.com/sites/home"

Add-Type -Path "C:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.dll"
Add-Type -Path "C:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.Runtime.dll"
#Add-Type -Path "C:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.UserProfiles.dll"

Connect-SPOCSOM -Username $User -Url https://earthnetworks1.sharepoint.com/sites/home/hr

#Get-SPOListItems Helpdesk -IncludeAllProperties $true -Recursive | where {($_.FSObjType -eq 0) -and ($_.IsCheckedoutToLocal -eq 1)}

$files = (Get-SPOListItems HR-Internal -IncludeAllProperties $true -Recursive | where {($_.FSObjType -eq 0) -and ($_.CheckoutUser -ne $null)})

$Files | select FileRef,CheckoutUser
