﻿$BlankEmails = Get-ADUser -Filter * -Properties UserPrincipalName,EmailAddress | where {($_.Enabled -eq $True) -and ($_.UserPrincipalName -like "*@earthnetworks.com") -and ($_.EmailAddress -eq $null)} | sort Name | Select Name,UserPrincipalName,EmailAddress

foreach ($blank in $BlankEmails)
{
    $primarySMTP = Get-MsolUser -UserPrincipalName $blank.UserPrincipalName -ErrorAction SilentlyContinue | select UserPrincipalName 
    
    if($PrimarySMTP.userprincipalname -ne $null)
    {
        Write-Host $blank.Name 'address is'$PrimarySMTP.userprincipalname
    }
}
