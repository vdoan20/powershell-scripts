﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2018 v5.5.148
	 Created on:   	2/14/2018 9:58 PM
	 Created by:   	Victor Doan
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>

$dcs = "SRXAD14", "SRXAD15", "SRXAD12", "SRXAD13"
$svcs = "adws", "dns", "kdc", "netlogon"
$data = Get-Service -name $svcs -ComputerName $dcs

$ReportTitle = "Domain Controller Services"

$head = @"
<Title>$ReportTitle</Title>
<style>
body { background-color:#FFFFFF;
font-family:Tahoma;
font-size:12pt; }
td, th { border:1px solid black;
border-collapse:collapse; }
th { color:white;
background-color:black; }
table, tr, td, th { padding: 2px; margin: 0px }
table { width:95%;margin-left:5px; margin-bottom:20px;}
.stopped {color: Red }
.running {color: Green }
.pending {color: #DF01D7 }
.paused {color: #FF8000 }
.other {color: Black }
</style>
<br>
<H1>$ReportTitle</H1>
"@

[xml]$html = $data |
Select @{ Name = "DomainController"; Expression = { $_.MachineName.ToUpper() } }, Name, Displayname, Status |
ConvertTo-Html -Fragment

1 .. ($html.table.tr.count - 1) | foreach {
	#enumerate each TD
	$html.table.tr[$_]
}
1 .. ($html.table.tr.count - 1) | foreach {
	#enumerate each TD
	$td = $html.table.tr[$_]
	$td.childnodes
}
1 .. ($html.table.tr.count - 1) | foreach {
	#enumerate each TD
	$td = $html.table.tr[$_]
	$td.childnodes.item(3)
}
1 .. ($html.table.tr.count - 1) | foreach {
	#enumerate each TD
	$td = $html.table.tr[$_]
	#create a new class attribute
	$class = $html.CreateAttribute("class")
	
	#set the class value based on the item value
	Switch ($td.childnodes.item(3).'#text')
	{
		"Running" { $class.value = "running" }
		"Stopped" { $class.value = "stopped" }
		"Pending" { $class.value = "pending" }
		"paused" { $class.value = "paused" }
		Default { $class.value = "other" }
	}
	#append the class
	$td.childnodes.item(3).attributes.append($class) | Out-Null
}

ConvertTo-HTML -Head $head -Body $html.InnerXml -PostContent "<h6>Created $(Get-Date)</h6>" |
Out-File -filepath c:\temp\DCSvcs.htm -Encoding asci