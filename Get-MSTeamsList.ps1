﻿Write-Host "Fetching list of groups..."
$Groups = Get-UnifiedGroup
Write-Host "Starting to process" $Groups.Count "groups"
$Count = 0
ForEach ($G in $Groups) {
   $Test = (Get-MailboxFolderStatistics -Identity $G.Alias -FolderScope ConversationHistory -IncludeOldestAndNewestItems)
   If ($Test -eq $Null) {
      Write-Host $G.DisplayName "no Conversation History folder found in mailbox"
      Write-Host $Test }
   Else {
       If (($Test.FolderType[1] -eq "TeamChat") -and ($Test.ItemsInFolder[1] -gt 0)) 
       { Write-Host $G.DisplayName "is team-enabled" 
          $Count++ }
       }
}
Write-Host $Count "of" $Groups.Count "are enabled for Teams"