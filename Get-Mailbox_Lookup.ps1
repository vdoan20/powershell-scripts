﻿$Test = Get-Mailbox -ErrorAction SilentlyContinue

if($?)
{
    
}
else
{
    $cred = Get-Credential
    $Exchange = New-PSSession –ConfigurationName microsoft.exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $cred -Authentication Basic  -AllowRedirection
    Import-PSSession $Exchange
}

$Name = Read-Host -Prompt "Email mailbox"
$mailbox = Get-Mailbox | where {$_.displayname -like "$Name*"}
$menu = @{}
for ($i=1;$i -le $mailbox.count; $i++) {
    Write-Host "$i. $($mailbox[$i-1].DisplayName)"
    $menu.Add($i,($mailbox[$i-1].DisplayName))
    }

[int]$ans = Read-Host 'Enter selection'
$selection = $menu.Item($ans)

Get-Mailbox $selection | select DisplayName,PrimarySmtpAddress | sort DisplayName | Format-Table -AutoSize
