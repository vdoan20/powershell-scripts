﻿$awscred =  Get-Credential -Message "Please enter AWS username, and password" -UserName "awscorp\$($env:USERNAME)"

$first, $last = (read-host "Input the first and last name") -split " "

$Name =  $first+" "+$last

$AWSUA =  Get-ADUser -Server "RDC-VAA01.awscorp.com" -Filter 'cn -eq $Name' -Properties * -Credential $awscred

if ($AWSUA.Enabled -eq $true)
    {
    Set-ADUser $AWSUA.SamAccountName -Enabled $false -Credential $awscred
    Write-Host "$($AWSUA.DisplayName) has been disabled." -ForegroundColor Yellow
    }
Else
    {
    Write-Host "$($AWSUA.DisplayName) is already disabled." -ForegroundColor Yellow
    }

foreach ($group in ($AWSUA | Select-Object -ExpandProperty Memberof))
        {
            Remove-ADGroupMember -Identity $group -Members $AWSUA.SamAccountName
            Write-Host $AWSUA.DisplayName "has been removed from" (Get-ADGroup $group | select -ExpandProperty Name) -ForegroundColor Yellow
        }