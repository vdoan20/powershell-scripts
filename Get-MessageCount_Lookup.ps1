﻿#$Test = Get-Mailbox -ErrorAction SilentlyContinue

if(Get-Command get-mailbox -ErrorAction SilentlyContinue)
{
    
}
else
{
    $cred = Get-Credential
    $Exchange = New-PSSession –ConfigurationName microsoft.exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $cred -Authentication Basic  -AllowRedirection
    Import-PSSession $Exchange
}

$Test = Get-MsolDomain -ErrorAction SilentlyContinue

if($?)
{
    
}
else
{
    #$cred = Get-Credential
    Connect-MsolService -Credential $cred
}

$Name = Read-Host -Prompt "Enter Group Name"
$mailbox = Get-MsolGroup | where {$_.displayname -like "*$Name*"} | Select DisplayName,EmailAddress | sort DisplayName
$menu = @{}
for ($i=1;$i -le $mailbox.count; $i++) {
    Write-Host "$i. $($mailbox[$i-1].DisplayName)"
    $menu.Add($i,($mailbox[$i-1].EmailAddress))
    }

[int]$ans = Read-Host 'Enter selection'
$selection = $menu.Item($ans)

(Get-MessageTrace -RecipientAddress $selection -StartDate ((get-date).adddays(-30)) -EndDate (get-date)).count
