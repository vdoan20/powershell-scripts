﻿$first, $last = (read-host "Input the first and last name") -split " "

$Name =  $first+" "+$last

$UA =  Get-ADUser -Filter "cn -eq '$($Name)'" -Properties *

$awscred =  Get-Credential -Message "Please enter AWS username, and password" -UserName "awscorp\$($env:USERNAME)"

#Testing Office 365 connection 

try
{
    Get-MsolDomain -ErrorAction Stop > $null
}
catch 
{
    if ($cred -eq $null) {$cred = Get-Credential $O365Adminuser}
    Write-Output "Connecting to Office 365..."
    Connect-MsolService -Credential $cred
}

$365User = Get-MsolUser -UserPrincipalName $UA.UserPrincipalName

#Disabling CORP domain account

if ($UA.Enabled -eq $true)
    {
    Set-ADUser $UA.SamAccountName -Enabled $false
    Write-Host "$($UA.DisplayName) has been disabled." -ForegroundColor Yellow
    }
Else
    {
    Write-Host "$($UA.DisplayName) is already disabled." -ForegroundColor Yellow
    }

#Blocking sign in to Office 365

if ($365User.BlockCredential -eq $False)
    {
    Set-MsolUser -UserPrincipalName $UA.UserPrincipalName -BlockCredential $true
    Write-Host "$($UA.DisplayName) has been blocked from signing in." -ForegroundColor Yellow
    }
Else
    {
    Write-Host "$($UA.DisplayName) has already been blocked from signing in." -ForegroundColor Yellow
    }

#Removing user from CORP AD groups

foreach ($group in ($UA | Select-Object -ExpandProperty Memberof))
        {
            Remove-ADGroupMember -Identity $group -Members $UA.SamAccountName
            Write-Host $Name "has been removed from" (Get-ADGroup $group | select -ExpandProperty Name) -ForegroundColor Yellow
        }

#Get all Office 365 groups

$groups = Get-MsolGroup -All 

#Removing user from office 365 groups

Write-Output "Removing user from Office 365 Groups..."
foreach ($group in $groups)
    {
    if (Get-MsolGroupMember -GroupObjectId $group.ObjectId | where {$_.EmailAddress -contains $UA.UserPrincipalName})
        {
        Remove-MsolGroupMember -GroupObjectId $group.ObjectId -GroupMemberObjectId $UA.UserPrincipalName
        Write-Host "$($UA.DisplayName) has been removed from $($group.DisplayName)" -ForegroundColor Yellow
        }
    }

#Disabling AWSCORP domain account

Try
{
    $AWSUA =  Get-ADUser -Server "RDC-VAA01.awscorp.com" -Filter "cn -eq $Name" -Properties * -Credential $awscred
}
catch
{
    if ($AWSUA -eq $null)
    {
        Write-Host "The is no user account for $($Name) in AWSCORP" -ForegroundColor Yellow
    }
    Else
    {
        if ($AWSUA.Enabled -eq $true)
        {
            Set-ADUser $AWSUA.SamAccountName -Enabled $false -Credential $awscred
            Write-Host "$($AWSUA.DisplayName) has been disabled." -ForegroundColor Yellow
        }
        
        #Removing user from AWS AD groups
        
        elseif(($AWSUA | select -ExpandProperty Memberof) -ne $null)
        {
          foreach ($group in ($AWSUA | Select-Object -ExpandProperty Memberof))
            {
                Remove-ADGroupMember -Identity $group -Members $AWSUA.SamAccountName
                Write-Host $AWSUA.DisplayName "has been removed from" (Get-ADGroup $group | select -ExpandProperty Name) -ForegroundColor Yellow
            }  
        }

        Else
        {
            Write-Host "$($AWSUA.DisplayName) is already disabled." -ForegroundColor Yellow
        }
    }
}

<#
foreach ($group in ($AWSUA | Select-Object -ExpandProperty Memberof))
        {
            Remove-ADGroupMember -Identity $group -Members $AWSUA.SamAccountName
            Write-Host $AWSUA.DisplayName "has been removed from" (Get-ADGroup $group | select -ExpandProperty Name) -ForegroundColor Yellow
        }
#>