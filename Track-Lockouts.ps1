﻿#Collect lockout accounts from ADS

$days = Read-host "Days to track"
$totaldays =  "-"+$days
$logname = "security"
$dcname = (Get-AdDomain).pdcemulator
$eventID = "4740"
$content = Get-EventLog -LogName $logname -ComputerName $dcname -After (Get-Date).AddDays($totaldays) -InstanceId $eventID | Select TimeGenerated, ReplacementStrings
$ofs = "`r`n`r`n"
$body = "Fetching event log started on " + (Get-Date) + $ofs

If ($content -eq $null)
{
    $body = $body + "No lock-out accounts happened today" + $ofs
}
Else 
{
    Foreach ($event in $content)
    {
        $source = $content.ReplacementStrings[1]
        $username = $content.ReplacementStrings[0]
        $body = $body + $event.TimeGenerated + ": " + $username + " - " + $source + $ofs
    }
}
$body