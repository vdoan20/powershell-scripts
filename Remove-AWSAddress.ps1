﻿$mailboxes = get-mailbox -filter * | where {($_.EmailAddresses -like "*@aws.com") -and ($_.Alias -ne "97c4664496ec4035a5a9f5fbb13802c7bob") -and ($_.IsDirSynced -ne $true)} | select Alias,Emailaddresses

foreach ($mailbox in $mailboxes)
    {
        $pr = $mailbox.Emailaddresses -like "*@aws.com"
        Write-host "Removing $($pr)"
        Set-Mailbox -identity $mailbox.alias -EmailAddresses @{remove="$($pr)"} -Verbose
        Write-host "$($pr) has been removed"
    }