﻿# Remote System Information
# Shows hardware and OS details from a list of PCs
# Thom McKiernan 22/08/2014
# Modified to suit my needs by Victor Doan 09/02/2018

$ArrComputers =  Get-ADComputer -Filter * -Properties * | where {($_.OperatingSystem -like "*Windows 7*") -and ($_.Enabled -eq $true)} | sort Name
#Specify the list of PC names in the line above. "." means local system

Clear-Host
foreach ($Computer in $ArrComputers) 
{
              if ((Test-Connection -ComputerName $Computer.Name -Count 1 -Quiet) -eq $true)
                 
                 {
                        $computerSystem = get-wmiobject Win32_ComputerSystem -computer $Computer.Name
                        $computerBIOS = get-wmiobject Win32_BIOS -Computer $Computer.Name
                        $computerOS = get-wmiobject Win32_OperatingSystem -Computer $Computer.Name
                        $computerCPU = get-wmiobject Win32_Processor -Computer $Computer.Name
                        $computerHDD = Get-WmiObject Win32_LogicalDisk -ComputerName $Computer | where {$_.DeviceId -eq "C:"}
                        #$computerdescription = Get-ADComputer $Computer -Properties Description | select Description
                            write-host "System Information for: " $computer.Name -BackgroundColor DarkCyan
                            "-------------------------------------------------------"
                            "Manufacturer: " + $computerSystem.Manufacturer
                            "Model: " + $computerSystem.Model
                            "Serial Number: " + $computerBIOS.SerialNumber
                            "CPU: " + $computerCPU.Name
                            "HDD Capacity: "  + "{0:N2}" -f ($computerHDD.Size/1GB) + "GB"
                            "HDD Space: " + "{0:P2}" -f ($computerHDD.FreeSpace/$computerHDD.Size) + " Free (" + "{0:N2}" -f ($computerHDD.FreeSpace/1GB) + "GB)"
                            "RAM: " + "{0:N2}" -f ($computerSystem.TotalPhysicalMemory/1GB) + "GB"
                            "Operating System: " + $computerOS.caption + ", Service Pack: " + $computerOS.ServicePackMajorVersion
                            "User logged In: " + $computerSystem.UserName
                            "Last Reboot: " + $computerOS.ConvertToDateTime($computerOS.LastBootUpTime)
                            "Computer Description: " +  $computerdescription.Description
                            ""
                            "-------------------------------------------------------"                                  
                 }
                 Else
                    {
                        Write-Host "$($Computer.Name) cannot be reached" -ForegroundColor Yellow
                    }
}
