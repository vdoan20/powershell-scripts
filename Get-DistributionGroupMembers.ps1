﻿$groups = Get-DistributionGroup -Filter * | select DisplayName,Alias,ManagedBy


foreach ($group in $groups)
    {
        Write-Host "Users in $($group.Displayname):" -ForegroundColor Yellow
        Write-host "$($group.Alias) is managed by $($group.ManagedBy)" -ForegroundColor Green
        Get-DistributionGroupMember $group.Alias
    }