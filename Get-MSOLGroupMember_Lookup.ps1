﻿

$Test = Get-MsolDomain -ErrorAction SilentlyContinue

if($?)
{
    
}
else
{
    $cred = Get-Credential
    Connect-MsolService -Credential $cred
}

$Name = Read-Host -Prompt "Enter Group Name"
$mailbox = Get-MsolGroup | where {$_.displayname -like "*$Name*"} | Select DisplayName,EmailAddress,ObjectId | sort DisplayName
$menu = @{}
for ($i=1;$i -le $mailbox.count; $i++) {
    Write-Host "$i. $($mailbox[$i-1].DisplayName)"
    $menu.Add($i,($mailbox[$i-1].ObjectId))
    }

[int]$ans = Read-Host 'Enter selection'
$selection = $menu.Item($ans)

Get-MsolGroupMember -GroupObjectId $selection | select DisplayName,EmailAddress