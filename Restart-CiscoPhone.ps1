﻿$IP = Read-Host "Enter phone's IP Address"

#Invoke-WebRequest -Uri "http://$($IP)/admin/reboot"

If (!(Test-Connection $IP -Count 2 -ErrorAction SilentlyContinue))
{
    Write-Host "Cannot find this phone." -ForegroundColor Red
}
Else
{
    Invoke-WebRequest -Uri "http://$($IP)/admin/reboot"
    if ((Invoke-WebRequest -Uri "http://$($IP)/admin/reboot" | Out-Null).StatusDescription -eq "OK")
    {
        Write-Host "Phone $($IP) has been rebooted." -ForegroundColor Yellow
    }
    else
    {
        Write-Host "Something went wrong." -ForegroundColor Red
    }
}

