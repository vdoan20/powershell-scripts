﻿$date = Get-Date

$old = $date.AddDays(-183)

get-adcomputer -Filter * -Properties * | Where {($_.name -like 'wsx*') -and ($_.Enabled -eq $true) -and ($_.LastLogondate -lt $old )}  | select Name,Description,Lastlogondate,OperatingSystem | sort Name -Descending | Format-Table -AutoSize