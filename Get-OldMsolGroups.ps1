﻿<#
1. Check for Exchange, and MSOLOnline modules

2. Get a list of all groups in Office 365

3. Run message trace for the last 30 days.

4. If the group has has no messages in the last 30 days get the following informaiton"
        
        a. Group Name
        b. Group Manager
        c. Group Members
        d. Group Type
        e. Group Email Address

#>


if(Get-Command Get-Mailbox -ErrorAction SilentlyContinue)
{
    
}
else
{
    $cred = Get-Credential
    $Exchange = New-PSSession –ConfigurationName microsoft.exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $cred -Authentication Basic  -AllowRedirection
    Import-PSSession $Exchange
}

if(Get-Command Get-MsolDomain -ErrorAction SilentlyContinue)
{
    
}
else
{
    Import-Module MSOnline -Global -Force
}

$Test = Get-MsolDomain -ErrorAction SilentlyContinue

if($?)
{
    
}
else
{
    Connect-MsolService -Credential $cred
}

$groups = Get-MsolGroup -all | where {$_.EmailAddress -ne $null}

$colOldGroups = @()

foreach ($group in $groups)
{
    if($group.EmailAddressaddress -ne $null)
    {
        $userObj = New-Object System.Object
        if((Get-MessageTrace -RecipientAddress $Address -StartDate ((get-date).adddays(-30)) -EndDate (get-date)).count -eq 0)
        {
            $Name=$group.DisplayName
            $Type=$group.GroupType
            $Id=$group.ObjectId
            $Address=$group.EmailAddress
            $Manager=$group.ManagedBy
            $userObj | Add-Member -Type NoteProperty -Name Name -Value $Name
            $userObj | Add-Member -Type NoteProperty -Name EmailAddress -Value $Address
            $userObj | Add-Member -Type NoteProperty -Name Manager -Value $Manager
            $userObj | Add-Member -Type NoteProperty -Name ID -Value $Id
            $userObj | Add-Member -Type NoteProperty -Name GroupType -Value $Type
        }

    }

    $colOldGroups += $userObj
}

$colOldGroupsCount = ($colOldGroups | Measure-Object).Count

$colOldGroupsCount

