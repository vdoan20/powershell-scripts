﻿$finds = Get-MsolGroup -GroupType DistributionList | sort DisplayName | select DisplayName,EmailAddress 

foreach ($Find in $finds) 

{
   Write-Host $Find.displayname -ForegroundColor Yellow
   (Get-MessageTrace -RecipientAddress $Find.EMailAddress -StartDate ((get-date).adddays(-30)) -EndDate (get-date)).count

}