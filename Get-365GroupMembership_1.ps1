﻿Connect-MsolService

$groups = Get-MsolGroup -All | select *

$Table = @()

$Record = 
@{
  "Group Name" = ""
  "Group Member Type" = ""
  "Name" = ""
  "Email Address" = ""
}

foreach ($group in $groups)
{

$Arrayofmembers = Get-MsolGroupMember -GroupObjectId $group.ObjectId | select GroupMemberType,Displayname,EmailAddress

foreach ($Member in $Arrayofmembers)
{
$Record."Group Name" = $Group.DisplayName
$Record."Group Member Type" = $Member.GroupMemberType
$Record."Name" = $Member.Displayname
$Record."Email Address" = $member.EmailAddress
$objRecord = New-Object PSObject -property $Record
$Table += $objrecord
}

}

$Table |  export-csv "C:\temp\Office365Groups.csv" -NoTypeInformation