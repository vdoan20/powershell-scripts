﻿$whiskcomputers = Get-ADComputer -Filter * -SearchBase "OU=Computers,OU=Whisker Labs,DC=CORP,DC=EN,DC=COM" | select Name | sort Name

Foreach ($whiskcomputer in $whiskcomputers)
{
    $name = $whiskcomputer.name
    #New-PSSession -ComputerName $name -Name $name
    $name
  Try
  {
  $test = Invoke-Command -computername $name -ScriptBlock {Get-Tpm} -ErrorAction SilentlyContinue -ErrorVariable ProcessError
    If ($Test.TPMReady -eq $true)
    {
       Invoke-Command -ComputerName $name -ScriptBlock {Manage-bde -status C:} 
    }
    If ($test.TPMReady -eq $false)
    {
        Write-Host 'System is not ready for BitLocker!' -ForegroundColor Yellow
    }    
    If ($ProcessError)
    {
        Write-Warning -Message "Something went wrong!"
    }
   }
   Catch
   {
   } 
 
 }
