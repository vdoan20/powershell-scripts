﻿$first, $last = (read-host "Input the first and last name") -split " "

$Name =  $first+" "+$last

$UA =  Get-ADUser -Filter 'cn -eq $Name' -Properties *

try
{
    Get-MsolDomain -ErrorAction Stop > $null
}
catch 
{
    if ($cred -eq $null) {$cred = Get-Credential $O365Adminuser}
    Write-Output "Connecting to Office 365..."
    Connect-MsolService -Credential $cred
}

$groups = Get-MsolGroup -All 

foreach ($group in $groups)
    {
    if (Get-MsolGroupMember -GroupObjectId $group.ObjectId | where {$_.EmailAddress -contains $UA.UserPrincipalName})
        {
        #Remove-MsolGroupMember -GroupObjectId $group.ObjectId -GroupMemberObjectId $UA.UserPrincipalName
        Write-Host "$($UA.DisplayName) is a member of $($group.DisplayName)"
        }
    }
