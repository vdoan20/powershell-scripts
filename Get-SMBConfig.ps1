﻿$comp = Read-Host -Prompt "Computername"

Invoke-Command -ComputerName $comp -ScriptBlock {Get-WindowsOptionalFeature –Online –FeatureName SMB1Protocol | select FeatureName,State ; Get-SmbServerConfiguration | select EnableSMB2Protocol}
