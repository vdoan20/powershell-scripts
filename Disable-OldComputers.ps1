﻿<##
This scripts looks for all computers that have not as a password reset in over 190.
It will then disable the computer, and change the description field to show when that change happened. 
Matt Bamba 4/25/2018
##>

Write-Warning -Message "This script will disable all computers that have not had a password update in 190 days."

$Value = Read-Host " Do you want to continue? (Y/N) "
Switch ($value){
    'Y' {'Now performing operation'}
    'N' {'You have chosen to exit the script' ; exit}
    }

$date = get-date

$StaleComputers = Get-ADComputer -Filter * -Properties * | where {($_.Enabled -eq $true) -and ($_.PasswordLastSet -le ($date.AddDays(-190)))}

foreach ($StaleComputer in $StaleComputers)

{
    Write-Host "Disabling $($StaleComputer)!" -ForegroundColor Red
    Set-ADComputer -Identity $StaleComputer -Description "Disabled on $($date)" -Enabled $false
}