﻿<###################################################################### 
# Name : Remove Filtered DNS Records                                                                                                    
# Date         : 26th February 2015                             
# Version      : 1.0                                                 
# Description  : This script will remove the DNS Resource Records filtered with the following parameters.                                                    
######################################################################> 
#Parametreler istege gore degistirilebilir. 
Import-mode DNSServer 
$zone="cozumpark.local" 
$DNSServer="2008R2DC1" 
$beforedate="12/20/2014" 
$joker="2008R2" 
$recordtype="A" 
$records=Get-DnsServerResourceRecord -ZoneName "$zone" -ComputerName $DNSServer |  
Where-Object {$_.RecordType -eq "$recordtype" -and $_.HostName -like "$joker*" -and $_.TimeStamp -lt $beforedate}  
Foreach ($record in $records) 
{ 
# Remove the DNS record by filtering 
Try 
{ 
$hostadi=$record.HostName 
Remove-DnsServerResourceRecord -ZoneName $zone -ComputerName $DNSServer -Force -RRType "$recordtype" -Name $record.HostName 
Write-Host ("[{0}] deleted record name is : $hostadi" -f (Get-Date)) 
("[{0}] delete record name: $hostadi" -f (Get-Date)) | out-file "c:\DNSRecordRemoveLog.txt" -Append 
} 
Catch 
{ 
Write-Host ("[{0}] Cannot delete the record: $hostadi" -f (Get-Date)) 
("[{0}] Cannot delete the record name: $hostadi" -f (Get-Date)) | out-file "c:\DNSRecordRemoveLog.txt" -Append 
} 
} 