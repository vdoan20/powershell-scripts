﻿<#
This sample script is not supported under any standard support program or service. 
This sample sample script is provided AS IS without warranty of any kind. 
The author further disclaims all implied warranties including, without limitation, any implied warranties of merchantability 
or of fitness for a particular purpose. 
The entire risk arising out of the use or performance of the sample scripts and documentation remains with you. 
In no event shall the author, or anyone else involved in the creation, production, or delivery 
of the script be liable for any damages whatsoever (including, without limitation, damages for loss of business profits, 
business interruption, loss of business information, or other pecuniary loss) arising out of the use 
of or inability to use the sample scripts or documentation, even if the author has been 
advised of the possibility of such damages.
-Modified by Victor Doan 3/7/2019
#>

Function Write-Menu {
    [CmdletBinding()]
    [Alias()]
    [OutputType([int])]
    Param
    (
        # Param1 help description
        [Parameter(Mandatory = $true,
            ValueFromPipelineByPropertyName = $true,
            Position = 0)]
        [string]$Header = '',

        # Param2 help description
        [System.ConsoleColor]$HeaderColor = 'Yellow',

        [string]$Property = "",

        $Items
    )

    Begin {
    }
    Process {
        if ($Header -ne '') { 
            Write-Host "`n$Header" -ForegroundColor $HeaderColor
            $underLine = "-" * $Header.length
            Write-Host $underLine
        }

        for ($i = 0; $i -lt $Items.Count; $i++) {
            $lz = ($i + 1).ToString("000")
            Write-Host "[$lz]: $($Items[$i].$Property)"
        }

        # Wait for user input
        do {
            $selection = Read-Host "`nPlease make a selection (or 'q' to stop)"
            if ($selection -eq 'q') {
                Exit
            }
            else {
                if ([int]$selection -le $Items.Length) {
                    Return $Items[[int]$selection - 1]    
                }
            
            }
        } while ($true)
    }
    
    End {
    }
}
Function Write-LogToConsole([string]$msg) {
    $strDate = Get-Date -Format "hh:mm:ss"
    Write-Host "[*] [$strDate]`t$msg"  -ForegroundColor Yellow       
}
Function Get-AzureSession() {
    if ([string]::IsNullOrEmpty($(Get-AzureRmContext).Account)) {
        Write-LogToConsole "Connecting to Azure"
        Add-AzureRmAccount 
    }
    else {
        Write-LogToConsole "Connected to Azure [$(Get-AzureRmContext).Account)]"
    }
    
    Write-LogToConsole "Getting Azure Subscriptions"
    $AzureRmSubscriptions = Get-AzureRmSubscription 
    $Subscription = Write-Menu -Items $AzureRmSubscriptions -Header "Select Azure Subscription" -HeaderColor Green -Property "Name"
    Select-AzureRmSubscription -SubscriptionId $Subscription.Id | Out-Null  
    $Subscription.Id    
}
<#
.Synopsis
  Manage multiple virtual machine access using just in time access
.DESCRIPTION
   Invoke-JustInTimeAccess openes selected ports on multiple Azure VM machines
.EXAMPLE
   Invoke-JustInTimeAccess -ResourceGroupName RG01 -Hours 1 -AddressPrefix -UseCurrentIPAddress
.NOTES
    Author      :: Moti Bani - Moti.ba@hotmail.com 
	Version 1.0 :: 18-June-2018 :: [Release] :: Publicly available
#>
function Enable-JustInTimeAccess {
    [CmdletBinding()]
    [Alias()]
    [OutputType([int])]
    Param
    (
        #Resource Group Name
        [String]
        $ResourceGroupName,

        # Specify how many hours the ports should be enabled
        [double]
        $Hours,

        # Source IP Address Prefix. (IP Address, CIDR block, or *) Default = * (Any)
        [string]
        $AddressPrefix = '*',

        # Azure subscription ID
        [String]
        $SubscriptionId,

        # Ports to be enabled
        [int[]]$Port,

        # Use the current public IP Adress as source
        [switch] $UseCurrentIP
    )

    if([string]::IsNullOrWhitespace($SubscriptionId)) {
        $SubscriptionId = Get-AzureSession
    }

    if([string]::IsNullOrWhitespace($ResourceGroupName)) {
        $ResourceGroups = Get-AzureRmResourceGroup 
        $ResourceGroupName = (Write-Menu -Items $ResourceGroups -Header "Select resource group" -Property 'ResourceGroupName' -HeaderColor Green).ResourceGroupName
    }
    

    if (-not (Get-Module AzureRm.Profile)) {
        Import-Module AzureRm.Profile
    }

    Write-Verbose "Checking AzureRM.profile version"
    $azureRmProfileModuleVersion = (Get-Module AzureRm.Profile).Version

    # refactoring performed in AzureRm.Profile v3.0 or later
    if ($azureRmProfileModuleVersion.Major -ge 3) {
        Write-Verbose "AzureRM.profile v3.x verified"
        $azureRmProfile = [Microsoft.Azure.Commands.Common.Authentication.Abstractions.AzureRmProfileProvider]::Instance.Profile
        if (-not $azureRmProfile.Accounts.Count) {
            Write-Error "Ensure you have logged in before calling this function."
        }
    }

    $currentAzureContext = Get-AzureRmContext
    $profileClient = New-Object Microsoft.Azure.Commands.ResourceManager.Common.RMProfileClient($azureRmProfile)
  
    $token = $profileClient.AcquireAccessToken($currentAzureContext.Subscription.TenantId)
    $token = $token.AccessToken

    Set-Variable -Name requestHeader -Scope Script -Value @{"Authorization" = "Bearer $token"}

    
    if ($UseCurrentIP) {
        $AddressPrefix = Invoke-RestMethod 'http://ipinfo.io/json' | Select-Object -ExpandProperty ip
    }
    else {
        $AddressPrefix = '*'
    }

    $asc_endpoint = 'jitNetworkAccessPolicies' #Set endpoint.
    $location = (Get-AzureRmResourceGroup -resourcegroupname $ResourceGroupName).location

    $endtimeutc = [DateTime]::UtcNow.AddMinutes(($Hours*60)).toString("o", [CultureInfo]::InvariantCulture)
    Write-LogToConsole "Ports will be opened until $([System.TimeZone]::CurrentTimeZone.ToLocalTime($endtimeutc))"
    $Port_collection = @()
    foreach ($i in $Port) {
        $Port_collection += @{
            number                     = $i
            allowedSourceAddressPrefix = $AddressPrefix
            endTimeUtc                 = $endtimeutc
        }
    }


    $VMs = (Get-AzureRmVM -ResourceGroupName $ResourceGroupName)
    

    $AllVMs = @()
    foreach ($vm in $VMs) {
        $virtualMachine = @{
            id    = "/subscriptions/$SubscriptionId/resourceGroups/$ResourceGroupName/providers/Microsoft.Compute/virtualMachines/$($VM.Name)"
            ports = $Port_collection
        }
        $AllVMs += $virtualMachine
    }

    $Body = @{
        virtualMachines = $AllVMs
    }
       

    $JSON = $Body | ConvertTo-Json -Depth 4
    $version = "2015-06-01-preview"  
    $asc_APIVersion = "?api-version=$version" #Build version syntax.
    $asc_uri = "https://management.azure.com/subscriptions/$subscriptionId/resourceGroups/$ResourceGroupName/providers/microsoft.Security/locations/$location/$asc_endpoint/default/Initiate$asc_APIVersion" 


    try {
        $asc_request = Invoke-RestMethod -Uri $asc_uri -Method Post -Body $JSON -Headers $requestHeader -ContentType "application/json"
        Write-LogToConsole "Specified ports for $(($VMs | Select-Object -expand Name) -join ', ') have been opened for $Hours hours."
        Write-LogToConsole "Ports may take up to 1 minute to open."
    }

    Catch {
        Write-Error "$_" -ErrorAction Stop
    }

} 
    
    
Enable-JustInTimeAccess -ResourceGroupName $ResourceGroupName -Hours 1  -Port 3389 -UseCurrentIP

