﻿$group = Read-Host -Prompt "Group to get members"

$test = Get-DynamicDistributionGroup -Identity $group

Get-Recipient -RecipientPreviewFilter $test.RecipientFilter -OrganizationalUnit $test.RecipientContainer | Select Name,Title,Department | sort Department
