﻿$Distros = Get-DistributionGroup -Filter * | where {$_.EmailAddresses -like "*@aws.com"} | select DisplayName,PrimarySmtpAddress,EmailAddresses,Alias | sort Alias

foreach ($distro in $distros)
    {
        $pr = $distro.Emailaddresses.Substring(5)
        Write-host "$($distro.Alias)" -ForegroundColor Yellow
        Foreach ($p in $pr)
        {
            $Result = @()
            Write-Host "Checking"-NoNewline; Write-Host " $($p)" -ForegroundColor Cyan
            Write-host (Get-MessageTrace -RecipientAddress $p -StartDate ((get-date).adddays(-30)) -EndDate (get-date)).count -ForegroundColor Green
        }

    }