﻿$date = Get-Date
$past = $date.AddDays(-600)

$14 = Get-DnsServerResourceRecord -ComputerName WDC-DC-01 -RRType A -ZoneName npca.org | where {($_.TimeStamp -le $past) -and ($_.TimeStamp -ne $null)} | select Hostname,TimeStamp | sort TimeStamp
$15 = Get-DnsServerResourceRecord -ComputerName WDC-DC-02 -RRType A -ZoneName npca.org | where {($_.TimeStamp -le $past) -and ($_.TimeStamp -ne $null)} | select Hostname,TimeStamp | sort TimeStamp

Compare-Object -ReferenceObject $14 -DifferenceObject $15